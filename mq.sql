/*
 Navicat Premium Data Transfer

 Source Server         : 本地2
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : localhost:3306
 Source Schema         : mq

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 02/04/2022 12:39:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `price` double(10, 2) DEFAULT NULL,
  `con` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `tips` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `is_addr` int(11) NOT NULL,
  `is_num` int(10) DEFAULT NULL,
  `is_show_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES (2, '修复膏', 1.00, '这是一个修复膏', '一盒可以用10到14天', 1, 1, 1);
INSERT INTO `goods` VALUES (3, '无人直播教程', 198.00, '这是一个网盘', '教程采用的是：课程+软件+售后。价格是全部包含的。请放心下单、', 0, 0, 2);
INSERT INTO `goods` VALUES (5, '增大教程', 98.00, '这是一个增大教程云盘', '这是增大教程，每天练习半个小时左右，坚持两个月无效可退！！', 0, 0, 1);

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence`  (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
INSERT INTO `hibernate_sequence` VALUES (75);
INSERT INTO `hibernate_sequence` VALUES (75);

-- ----------------------------
-- Table structure for pay_order
-- ----------------------------
DROP TABLE IF EXISTS `pay_order`;
CREATE TABLE `pay_order`  (
  `id` bigint(20) NOT NULL,
  `close_date` bigint(20) NOT NULL,
  `create_date` bigint(20) NOT NULL,
  `is_auto` int(11) NOT NULL,
  `notify_url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `order_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `param` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `pay_date` bigint(20) NOT NULL,
  `pay_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `pay_url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `price` double NOT NULL,
  `really_price` double NOT NULL,
  `return_url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `state` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `goods_id` int(10) DEFAULT NULL,
  `order_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `order_addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `goods_num` int(10) DEFAULT NULL,
  `goods_con` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `goods_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pay_order
-- ----------------------------
INSERT INTO `pay_order` VALUES (36, 1648471906532, 1648471592992, 1, NULL, '202203282046321723', '', 0, '1648471592948', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 1, 0.93, NULL, -1, 1, 2, '15516111415', '金水区未来路', 1, NULL, NULL);
INSERT INTO `pay_order` VALUES (37, 1648471936523, 1648471620398, 1, NULL, '202203282047006100', '', 0, '1648471620344', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 1, 0.92, NULL, -1, 1, 2, '15516111415', '金水区未来路', 2, NULL, NULL);
INSERT INTO `pay_order` VALUES (38, 1648471966529, 1648471641299, 1, NULL, '202203282047215596', '', 0, '1648471641274', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 198, 198, NULL, -1, 1, 3, '15516111415', '0', 2, NULL, NULL);
INSERT INTO `pay_order` VALUES (39, 1648704393391, 1648704079364, 1, NULL, '202203311321198249', '', 0, '1648704079272', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 1, 0.93, NULL, -1, 1, 2, '11111', 'sdsadasdsaaaaaa', 1, NULL, NULL);
INSERT INTO `pay_order` VALUES (40, 1648704513391, 1648704189418, 1, NULL, '202203311323093119', '', 0, '1648704189382', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 1, 0.92, NULL, -1, 1, 2, '11111', 'sdsadasdsaaaaaa', 1, NULL, NULL);
INSERT INTO `pay_order` VALUES (41, 1648704573385, 1648704261337, 1, NULL, '202203311324216595', '', 0, '1648704261299', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 1, 0.91, NULL, -1, 1, 2, '11111', 'ssssdadasdadasdadsad', 1, NULL, NULL);
INSERT INTO `pay_order` VALUES (42, 1648704491800, 1648704448090, 1, NULL, '202203311327283994', '', 1648704491800, '1648704448805', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 1, 1, NULL, 2, 2, 2, '6688664448', 'hjbbmjivvvbn', 1, NULL, NULL);
INSERT INTO `pay_order` VALUES (43, 1648706508884, 1648706197976, 1, NULL, '202203311356373722', '', 0, '1648706197891', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 1, 0.93, NULL, -1, 1, 2, '8856665522', 'ddddssdadada', 1, NULL, NULL);
INSERT INTO `pay_order` VALUES (44, 1648711348707, 1648711045745, 1, NULL, '202203311517257625', '', 0, '1648711045676', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 1, 0.93, NULL, -1, 1, 2, '15516111415', '金水区未来路ssssss', 1, NULL, NULL);
INSERT INTO `pay_order` VALUES (45, 1648711110360, 1648711076060, 1, NULL, '202203311517561044', '', 1648711110360, '1648711076037', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 1, 1, NULL, 2, 2, 2, '15516111415', '金水区未来路ssssss', 1, '', '第四个');
INSERT INTO `pay_order` VALUES (46, 1648712992479, 1648712904167, 1, NULL, '202203311548242725', '', 1648712992479, '1648712904121', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 1, 1, NULL, 2, 2, 2, '15516111415', '金水区未来路ssssss', 1, NULL, '第三个');
INSERT INTO `pay_order` VALUES (47, 1648713606335, 1648713303422, 1, NULL, '202203311555037973', '', 0, '1648713303376', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 1, 1, NULL, 1, 2, 2, '15516111415', '金水区未来路ssssssdd', 1, '这个是咱家的百度网盘，就是这么长一串。你向后看，真的还有很多内，不信你点开看看。', 'dierge');
INSERT INTO `pay_order` VALUES (48, 1648713892058, 1648713863406, 1, NULL, '202203311604235554', '', 1648713892058, '1648713863345', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 1, 1, NULL, 2, 2, 2, '15516111415', '金水区未来路ssssssdd', 1, '这是一个修复膏', '第一个');
INSERT INTO `pay_order` VALUES (50, 1648715141946, 1648714821161, 1, NULL, '202203311620214424', '', 0, '1648714821108', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 1, 1, NULL, -1, 2, 2, '15516111415', '金水区未来路eeee', 1, NULL, '修复膏');
INSERT INTO `pay_order` VALUES (51, 1648784715266, 1648784392904, 1, NULL, '202204011139524893', '', 0, '1648784392316', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 198, 198, NULL, -1, 1, 3, '15516111415', '0', 1, NULL, '无人直播教程');
INSERT INTO `pay_order` VALUES (52, 1648788915268, 1648788594827, 1, NULL, '202204011249542143', '', 0, '1648788594778', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 198, 198, NULL, -1, 1, 3, '15516111415', '0', 1, NULL, '无人直播教程');
INSERT INTO `pay_order` VALUES (53, 1648789095264, 1648788784056, 1, NULL, '202204011253043504', '', 0, '1648788784021', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 198, 198, NULL, -1, 2, 3, '15516111415', '0', 1, NULL, '无人直播教程');
INSERT INTO `pay_order` VALUES (54, 1648789095264, 1648788791224, 1, NULL, '202204011253117744', '', 0, '1648788791161', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 198, 197.99, NULL, -1, 2, 3, '15516111415', '0', 1, NULL, '无人直播教程');
INSERT INTO `pay_order` VALUES (55, 1648789995262, 1648789683929, 1, NULL, '202204011308039947', '', 0, '1648789683890', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 98, 98, NULL, -1, 2, 5, '15516111415', '0', 1, NULL, '增大教程');
INSERT INTO `pay_order` VALUES (56, 1648790055266, 1648789731606, 1, NULL, '202204011308516490', '', 0, '1648789731550', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 1, 0.93, NULL, -1, 1, 2, '15516111415', '0', 1, NULL, '修复膏');
INSERT INTO `pay_order` VALUES (57, 1648790055266, 1648789740190, 1, NULL, '202204011309009880', '', 0, '1648789740162', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 1, 1, NULL, -1, 2, 2, '15516111415', '0', 1, NULL, '修复膏');
INSERT INTO `pay_order` VALUES (58, 1648790385273, 1648790068220, 1, NULL, '202204011314282996', '', 0, '1648790068174', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 1, 1, NULL, -1, 2, 2, '15516111415', '0', 1, NULL, '修复膏');
INSERT INTO `pay_order` VALUES (59, 1648790415271, 1648790115112, 1, NULL, '202204011315152726', '', 0, '1648790114600', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 1, 0.99, NULL, -1, 2, 2, '123456789', '123456', 1, NULL, '修复膏');
INSERT INTO `pay_order` VALUES (60, 1648792305265, 1648791995733, 1, NULL, '202204011346351728', '', 0, '1648791995707', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 98, 98, NULL, -1, 1, 5, '', '0', 1, NULL, '增大教程');
INSERT INTO `pay_order` VALUES (61, 1648795858644, 1648795548535, 1, NULL, '202204011445482410', '', 0, '1648795548475', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 1, 0.93, NULL, -1, 1, 2, '111222333', '0', 1, NULL, '修复膏');
INSERT INTO `pay_order` VALUES (62, 1648795628521, 1648795556270, 1, NULL, '202204011445568567', '', 1648795628521, '1648795556245', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 1, 1, NULL, 2, 2, 2, '111222333', '0', 1, '这是一个修复膏', '修复膏');
INSERT INTO `pay_order` VALUES (67, 1648795895801, 1648795876485, 1, NULL, '202204011451165131', '', 1648795895801, '1648795876426', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 1, 1, NULL, 2, 2, 2, 'wwww', '金水区未来路', 1, '这是一个修复膏', '修复膏');
INSERT INTO `pay_order` VALUES (70, 1648796444431, 1648796416258, 1, NULL, '202204011500163786', '', 1648796444431, '1648796416192', 'https://qr.alipay.com/fkx10540odlian7xmakm96a', 1, 1, NULL, 2, 2, 2, '1111', '金水区未来路1111', 1, '这是一个修复膏', '修复膏');
INSERT INTO `pay_order` VALUES (74, 1648801888692, 1648801566339, 1, NULL, '202204011626063778', '', 0, '1648801566214', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW', 1, 0.93, NULL, -1, 1, 2, '0', '金水区未来路', 1, NULL, '修复膏');

-- ----------------------------
-- Table structure for pay_qrcode
-- ----------------------------
DROP TABLE IF EXISTS `pay_qrcode`;
CREATE TABLE `pay_qrcode`  (
  `id` bigint(20) NOT NULL,
  `pay_url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `price` double NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting`  (
  `vkey` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `vvalue` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`vkey`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of setting
-- ----------------------------
INSERT INTO `setting` VALUES ('user', 'admin');
INSERT INTO `setting` VALUES ('pass', 'admin');
INSERT INTO `setting` VALUES ('notifyUrl', 'ok.html');
INSERT INTO `setting` VALUES ('returnUrl', 'ok.html');
INSERT INTO `setting` VALUES ('key', '7c57f3ddf9a4fa93d649a8a2cbe5227c');
INSERT INTO `setting` VALUES ('lastheart', '1648797216996');
INSERT INTO `setting` VALUES ('lastpay', '1648796443970');
INSERT INTO `setting` VALUES ('jkstate', '0');
INSERT INTO `setting` VALUES ('close', '5');
INSERT INTO `setting` VALUES ('payQf', '2');
INSERT INTO `setting` VALUES ('wxpay', 'wxp://f2f1wEVTbSP5uehA2CTtan_sLM5uBuGBLTS7XqtURtiQyYiGpuaPXOZueSnE0Q_Iq_yW');
INSERT INTO `setting` VALUES ('zfbpay', 'https://qr.alipay.com/fkx10540odlian7xmakm96a');

-- ----------------------------
-- Table structure for tmp_price
-- ----------------------------
DROP TABLE IF EXISTS `tmp_price`;
CREATE TABLE `tmp_price`  (
  `price` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`price`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tmp_price
-- ----------------------------
INSERT INTO `tmp_price` VALUES ('1-0.94');
INSERT INTO `tmp_price` VALUES ('1-0.95');
INSERT INTO `tmp_price` VALUES ('1-0.96');
INSERT INTO `tmp_price` VALUES ('1-0.97');
INSERT INTO `tmp_price` VALUES ('1-0.98');
INSERT INTO `tmp_price` VALUES ('1-0.99');
INSERT INTO `tmp_price` VALUES ('1-1.0');

SET FOREIGN_KEY_CHECKS = 1;
