package com.vone.mq.dao;

import com.vone.mq.entity.Goods;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GoodsDao extends JpaRepository<Goods,String>{

    @Query(value = "select id,goods_name,price,tips,is_addr,is_num,is_show_id,con from goods", nativeQuery = true)
    List<Goods> findGoods();

    @Query(value = "select con from goods where id = ?1", nativeQuery = true)
    String getGoodsCon(int goodId);
}
