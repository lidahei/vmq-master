package com.vone.mq.service;

import com.vone.mq.dao.GoodsDao;
import com.vone.mq.dto.CommonRes;
import com.vone.mq.entity.Goods;
import com.vone.mq.entity.PayQrcode;
import com.vone.mq.utils.ResUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    public CommonRes findGoods() {
        List<Goods> all = goodsDao.findGoods();
        return ResUtil.success(all);
    }
    public CommonRes findGoodCon(int id) {
        String all = goodsDao.getGoodsCon(id);
        return ResUtil.success(all);
    }
}
