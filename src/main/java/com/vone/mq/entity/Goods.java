package com.vone.mq.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Goods {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String goodsName;

    private double price;

    private String tips;

    private int isAddr;

    private int isNum;

    private String con;

    public String getCon() {
        return con;
    }

    public void setCon(String con) {
        this.con = con;
    }


    public int getIsShowId() {
        return isShowId;
    }

    public void setIsShowId(int isShowId) {
        this.isShowId = isShowId;
    }

    private int isShowId;

    public int getIsNum() {
        return isNum;
    }

    public void setIsNum(int isNum) {
        this.isNum = isNum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }


    public int getIsAddr() {
        return isAddr;
    }

    public void setIsAddr(int isAddr) {
        this.isAddr = isAddr;
    }
}
